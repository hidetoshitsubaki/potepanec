require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let!(:taxon_1) { create(:taxon, name: "taxon_1") }
    let!(:taxon_2) { create(:taxon, name: "taxon_2") }
    let!(:product_1) { create(:product, taxons: [taxon_1], name: 'product_1') }
    let!(:product_2) { create(:product, taxons: [taxon_1], name: 'product_2') }
    let!(:product_3) { create(:product, taxons: [taxon_2], name: 'product_3') }

    before do
      get potepan_product_path product_1.id
    end

    it "リクエストが成功すること" do
      expect(response.status).to eq 200
    end

    it "選択した商品名が表示されていること" do
      expect(response.body).to include product_1.name
    end

    it "関連商品が表示されていること" do
      expect(response.body).to include product_2.name
    end

    it "関連していない商品が表示されないこと" do
      expect(response.body).not_to include product_3.name
    end
  end
end
