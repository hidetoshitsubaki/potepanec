require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan_categories/show" do
    let!(:taxonomy_1) { create(:taxonomy, name: 'Category') }
    let!(:taxonomy_2) { create(:taxonomy, name: 'Brand') }
    let!(:root_1) { taxonomy_1.root }
    let!(:root_2) { taxonomy_2.root }
    let!(:parent_1) { create(:taxon, name: 'Parent_1', taxonomy_id: taxonomy_1, parent: root_1) }
    let!(:parent_2) { create(:taxon, name: 'Parent_2', taxonomy_id: taxonomy_2, parent: root_2) }
    let!(:child_1) { create(:taxon, name: 'child', taxonomy_id: taxonomy_1, parent: parent_1) }
    let!(:child_2) { create(:taxon, name: 'child', taxonomy_id: taxonomy_2, parent: parent_2) }
    let!(:leaf_taxon) { create(:taxon, name: 'child', taxonomy_id: taxonomy_1, parent: child_1) }
    let!(:product_1) { create(:product, taxons: [parent_1], name: 'product_1') }
    let!(:product_2) { create(:product, taxons: [leaf_taxon], name: 'product_2') }
    let!(:product_3) { create(:product, taxons: [child_2], name: 'product_3') }

    before do
      get potepan_category_path parent_1.id
    end

    it "リクエストが成功すること" do
      expect(response.status).to eq 200
    end

    it "選択したカテゴリの商品が表示されること" do
      expect(response.body).to include product_1.name
    end

    it "選択したカテゴリと間接的に繋がっている商品が表示されること" do
      expect(response.body).to include product_2.name
    end

    it "選択したカテゴリ以外の商品が表示されないこと" do
      expect(response.body).not_to include product_3.name
    end
  end
end
