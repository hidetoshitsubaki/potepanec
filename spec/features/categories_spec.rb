require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:taxonomy_1) { create(:taxonomy, name: 'Category') }
  given!(:taxonomy_2) { create(:taxonomy, name: 'Brand') }
  given!(:root_1) { taxonomy_1.root }
  given!(:root_2) { taxonomy_2.root }
  given!(:parent_1) { create(:taxon, name: 'Parent_1', taxonomy_id: taxonomy_1, parent: root_1) }
  given!(:parent_2) { create(:taxon, name: 'Parent_2', taxonomy_id: taxonomy_2, parent: root_2) }
  given!(:child_1) { create(:taxon, name: 'child', taxonomy_id: taxonomy_1, parent: parent_1) }
  given!(:child_2) { create(:taxon, name: 'child', taxonomy_id: taxonomy_2, parent: parent_2) }
  given!(:leaf_taxon) { create(:taxon, name: 'child', taxonomy_id: taxonomy_1, parent: child_1) }
  given!(:product_1) { create(:product, taxons: [parent_1], name: 'product_1') }
  given!(:product_2) { create(:product, taxons: [leaf_taxon], name: 'product_2') }
  given!(:product_3) { create(:product, taxons: [child_2], name: 'product_3') }

  background do
    visit "/potepan/product_grid_left_sidebar.html"
    click_on taxonomy_1.name
    click_on parent_1.name
  end

  scenario "選択したカテゴリーの商品が表示されること" do
    expect(page).to have_content(product_1.name)
  end

  scenario "選択したカテゴリーと間接的に繋がっている商品が表示されること" do
    expect(page).to have_content(product_2.name)
  end

  scenario "選択したカテゴリー以外の商品が表示されないこと" do
    expect(page).not_to have_content(product_3.name)
  end
end
