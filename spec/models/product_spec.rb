require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "scope" do
    let!(:taxon_1) { create(:taxon, name: "taxon_1") }
    let!(:taxon_2) { create(:taxon, name: "taxon_2") }
    let!(:product_1) { create(:product, taxons: [taxon_1], name: 'product_1') }
    let!(:product_2) { create(:product, taxons: [taxon_1], name: 'product_2') }
    let!(:product_3) { create(:product, taxons: [taxon_2], name: 'product_3') }

    describe 'except_main_product' do
      subject { Spree::Product.except_main_product(product_1.id) }

      it { is_expected.not_to include product_1 }
      it { is_expected.to include product_2 }
    end

    describe 'related_products' do
      subject { Spree::Product.related_products(taxon_ids) }

      let!(:taxon_ids) { product_1.taxon_ids }

      it { is_expected.to include product_2 }
      it { is_expected.not_to include product_3 }
    end
  end
end
