class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    taxon_ids = @product.taxon_ids
    @products = Spree::Product.
      includes([master: [:default_price, :images]]).
      except_main_product(@product.id).
      related_products(taxon_ids)
  end
end
