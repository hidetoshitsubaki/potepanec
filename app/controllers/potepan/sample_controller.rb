class Potepan::SampleController < ApplicationController
  def index
    @new_products = Spree::Product.
    with_price_and_images.
    descend_by_available.limit(10)
  end

  def product_grid_left_sidebar
    @taxonomies = Spree::Taxonomy.includes([root: :children])
    @products = Spree::Product.includes([master: [:default_price, :images]])
  end

  def product_list_left_sidebar
  end

  def cart_page
  end

  def checkout_step_1
  end

  def checkout_step_2
  end

  def checkout_step_3
  end

  def checkout_complete
  end

  def blog_left_sidebar
  end

  def blog_right_sidebar
  end

  def blog_single_left_sidebar
  end

  def blog_single_right_sidebar
  end

  def about_us
  end

  def tokushoho
  end

  def privacy_policy
  end
end
