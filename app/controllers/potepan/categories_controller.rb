class Potepan::CategoriesController < ApplicationController
  before_action :set_valiables

  def set_valiables
    @taxonomies = Spree::Taxonomy.all.includes(root: :children)
    @colors = Spree::OptionValue.presentation("Color")
    @sizes = Spree::OptionValue.presentation("Size")
  end

  def show
    @taxon = Spree::Taxon.find(params[:taxon_id])
    @category = @taxon.name
    @products = @taxon.all_products.includes(master: [:default_price, :images])
  end

  def color
    @category = "Color - " + params[:color]
    @products = Spree::Product.
    with_price_and_images.
    with_value(params[:color])
    render 'potepan/categories/show'
  end

  def size
    @category = "Size - " + params[:size]
    @products = Spree::Product.
    with_price_and_images.
    with_value(params[:size])
    render 'potepan/categories/show'
  end

  def sort
  @category = "Sort - " + params[:sort]
  sort_by(params[:sort])
  p params[:sort]
  end

  def seach
    
  end

  private
    def sort_by(params)
      case params
      when "descend_by_available"
        p "1"
        @products = Spree::Product.
        with_price_and_images.
        descend_by_available
      when "ascend_by_available"
        p "2"
        @products = Spree::Product.
        with_price_and_images.
        ascend_by_available
      when "descend_by_price"
        p "3"
        @products = Spree::Product.
        with_price_and_images.
        descend_by_master_price
      when "ascend_by_price"
        p "4"
        @products = Spree::Product.
        with_price_and_images.
        ascend_by_master_price
      end
    end

end
