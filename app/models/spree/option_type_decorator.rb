Spree::OptionValue.class_eval do
  scope :presentation, ->(name) do
    includes(:option_type).
    where(spree_option_types: {presentation: name}).
    pluck(:presentation).
    uniq
  end
end
