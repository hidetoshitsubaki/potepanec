Spree::Product.class_eval do
  scope :with_price_and_images, -> do
    includes([master: [:default_price, :images]])
  end
  scope :related_products, -> (taxon_ids) do
    joins(:taxons).where("spree_taxons.id IN (?)", taxon_ids).distinct
  end
  scope :except_main_product, -> (product_id) { where.not(id: product_id) }
  scope :with_value, -> (presentation) do
    includes(variants_including_master: :option_values).
    where(spree_option_values: {presentation: presentation})
  end
  scope :descend_by_available, -> do
    order(available_on: "DESC")
  end
  scope :ascend_by_available, -> do
    order(available_on: "ASC")
  end
end